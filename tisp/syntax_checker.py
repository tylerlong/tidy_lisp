# coding=utf-8
"""
    tisp.syntax_checker
    ~~~~~~~~~~~~~~~~~~~
    syntax checker for Tisp Language
"""
import re, string


def check_line_syntax(line):
    """Check the syntax of a single code line"""
    if line is None:
        raise ValueError('The code line must not be None')
    line = _normalize(line)
    for check_method in (_check_first_token, _check_round_brackets, ):
        result, message = check_method(line)
        if not result:
            return result, message
    return True, 'syntax valid'


str_pattern = re.compile(r'".+?"')
def _normalize(line):
    """normalize the code line to make the checking easier"""
    line = line.strip().replace(r'\\', '').replace(r'\"', ' ')
    return str_pattern.sub('s', line)


first_token_pattern = re.compile(r'[a-z<>\[=#:.*/+-]')
def _check_first_token(line):
    """check the first char of the line to make sure that the first token is a function
    ref: http://clojure.github.com/clojure/
    """
    if not first_token_pattern.match(line):
        return False, 'each line must start with a function'
    return True, None


def _check_round_brackets(line):
    """check round brackets to validate syntax"""
    round_brackets_count = 0
    for ch in line:
        if ch == '(':
            round_brackets_count += 1
            if round_brackets_count >= 2:
                return False, 'must not contain nested round brackets'
        elif ch == ')':
            round_brackets_count -= 1            
    if round_brackets_count != 0:
        return False, 'the number of left round brackets must match the number of right round brackets'
    return True, None
