# coding=utf-8
"""
    tisp.testsuite
    ~~~~~~~~~~~~~~
    unit tests for the tisp project
"""
import unittest
from tisp.syntax_checker import check_line_syntax


class MainTestCase(unittest.TestCase):
    """main test case"""
    def test_check_line_syntax(self):
        """testing checking syntax of tisp"""
        fixtures = (('(/ (/ 4 2) (/ 6 3))', False,),
                ('(/ 4 2)', False,),
                ('/ 4 2', True,),
                ('str "(" "("', True,),
                ('/ (/ 4 2) (/ 6 3))', False,),
                ('/ (/ 4 2) (/ 6 3)', True,),
                ('1 (/ 4 2) (/ 6 3)', False,),
                )
        for fixture in fixtures:
            assert check_line_syntax(fixture[0])[0] == fixture[1]


def run_testsuite():
    test_suite = unittest.TestSuite()
    test_suite.addTest(unittest.makeSuite(MainTestCase))
    unittest.TextTestRunner(verbosity = 1).run(test_suite)
