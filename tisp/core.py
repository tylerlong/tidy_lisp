# coding=utf-8
"""
    tisp.core
    ~~~~~~~~~
    core for the tisp language
"""

SUPPORT_DIALECTS = frozenset(('clojure',))


def tisp_to_lisp(source, dialect = 'clojure'):
    """Convert tisp source code to the source code of a dialect of lisp.
    Parameters:
        source: source code of tisp
        dialect: dialect of lisp.
    """
    if not dialect in SUPPORT_DIALECTS:
        raise ValueError('does not support list dialect {0}'.format(dialect))
    lines = source.splitlines()
    return ''
