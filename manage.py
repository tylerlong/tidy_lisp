# coding = utf-8
"""
    manage
    ~~~~~~
    the entry point for the application
"""
import sys
from toolkit_library.inspector import ModuleInspector


def run_tests():
    """Run unit tests"""
    from tisp.testsuite import run_testsuite
    run_testsuite()


if __name__ =='__main__':
    moduleInspector = ModuleInspector(sys.modules[__name__])
    moduleInspector.invoke(*sys.argv[1:])
